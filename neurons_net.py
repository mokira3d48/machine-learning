# -*- encoding : utf-8 -*

# author  : Dr Mokira
# created : 2020-06-13
# updated : 2020-06-15

from layer import Layer

class Net :

    def __init__( this, xc=None, topology=None, yc=None ) :
        this._layers = [];
        x_c          = xc is not None and xc or 1;

        if topology :
            x = 0;

            if xc is not None and xc > 0 :
                this._layers.append( Layer( xc=xc, yc=topology[0] ) );
                x = 1;

            for i in range( x, len( topology ) ) :
                this._layers.append( Layer( xc=topology[i - 1], yc=topology[i] ) );

            if yc is not None and yc > 0 :
                this._layers.append( Layer( xc=topology[-1], yc=yc ) ); 

        elif xc is not None and xc > 0 :
            if yc is not None and yc > 0 : this._layers.append( Layer( xc=xc, yc=yc ) );


    def addLayer( this, _layer_ ) :
        if type( _layer_ ) is Layer : this._layers.append( _layer_ );
        else                        : raise ValueError( 'Layer type is unknow!' );


    def layer( this, _ind_ ) :
        return this._layers[_ind_];


    def removeLayer( this, _i_ ) :
        del this._layers_[_i_];

    
    def __str__( this ) :
        msg = "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ NERONS NETWORK @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        
        for i, ly in enumerate( this._layers ) :
            msg += "%9d\n %s" % (i, ly);

        msg += "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"

        return msg;

    
    def feedforward( this, _x_ ) :
        y = _x_;

        for l in this._layers :
            y = l.feedforward( y );

        return y;

#end

if __name__ == '__main__' :
    net = Net( topology=[2, 2] );

#    net.addLayer( Layer( xc=3, yc=100 ) );
#    net.addLayer( Layer( xc=100, yc=1000 ) );
#    net.addLayer( Layer( xc=1000, yc=10 ) );

    print( net );
    print( net.feedforward( [ 1, 2] ) );
