# -*-encoding : utf-8 -*

# author  : Dr Mokira
# created : 2020-06-13
# updated : 2020-06-15

from neuron import Neuron
from math   import tanh

class Layer :

    def __init__( this, xc=1, yc=1, neurons=[], f=None, df=None ) :
        this._neurons = [];
        
        for n in neurons :
            if f  : n.f( f );
            if df : n.df( df );
            
            this._neurons.append( n );

        if len( neurons ) == 0 :
            for i in range( yc ) :
                n = Neuron( wc = xc );

                if f  : n.f( f );
                if df : n.df( df );

                this._neurons.append( n );
    

    def addNeuron( this, _n_=None ) :
        if _n_ is not None : this._neurons.append( _n_ );
        else :
            n  = this._neurons[-1];
            nn = Neuron( wc=len( n.w() ), f=n.f(), df=n.df() );

            this._neurons.append( nn );

    def neuron( this, _ind_ ) :
        return this._neurons[_ind_];


    def removeNeuron( this, _ind_ ) :
        del this._neurons[_ind_];


    def __str__( this ) :
        msg = "-------------------------------------------\n";
        for i, n in enumerate( this._neurons ) : msg += "%5d\t%s" % (i, n);
        msg += "\n-------------------------------------------";

        return msg;


    def feedforward( this, _x_ ) :
        y = [];

        for n in this._neurons :
            y.append( n.feedforward( _x_ ) );

        return y;

#end


if __name__ == '__main__' :
    l = Layer( xc=2, neurons=[Neuron( W=[4, 5] ),
        Neuron( W=[3, 2], b=6, f=tanh )
        ] );
    
    l.addNeuron();

    print( l );
    print( l.feedforward( [2, 1] ) );
