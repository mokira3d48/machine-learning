import numpy    as np
import pandas   as pd

from sklearn import linear_model

# on charge le dataset :
hdata = pd.read_csv('./ressources/house.csv');


# on transforme le dataset en matrice :
X = np.matrix([np.ones(hdata.shape[0]), hdata['surface'].values]);
X = X.T;

Y = np.matrix(hdata['loyer']);
Y = Y.T;

# on initialize le model :
rg = linear_model.LinearRegression();

# on fait l'entrainement :
rg.fit(X, Y);

# on fait une prediction :
y = rg.predict(np.array([[35]]));
print(y);