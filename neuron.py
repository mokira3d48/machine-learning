# -*-encoding : utf-8 -*

# author  : Dr Mokira
# created : 2020-06-13
# updated : 2020-06-13

from random  import randint

class Neuron :

    def __init__( this, wc=1, W=[], b=0, f=( lambda x : x ), df=( lambda x : 1 ) ) :
        this._w  = [];
        this._b  = b;

        if len( W ) > 0 : this._w.extend( W );
        else            : this._w = [randint( 0, 100 )] * wc ;

        this._f  = f;
        this._df = df;

    
    def w( this, _w_=None ) :
        if   _w_ is None     : return this._w;
        else :
            this._w = _w_[:];


    def b( this, _b_=None ) :
        if not _b_                           : return this._b;
        elif type( _b_ ) in  ( int, float )  : this._b = _b_;
        else                                 : raise ValueError( 'b' );
    
    def f( this, _f_=None ) :
        if    not _f_     : return this._f;
        else              : this._f = _f_;
    
    def df( this, _df_=None ) :
        if    not _df_     : return this._df;
        else               : this._df = _df_;
    

    def __str__( this ) :
        return f"W = {this._w}\tb = {this._b}\tf = {this._f}\tdf = {this._df}\n";


    def feedforward( this, _x_ ) :
        y = 0;

        for i, w in enumerate( this._w ) :
            try :
                xi = _x_[i];
                y  = y + xi * w;

            except IndexError :
                return this._f( y + this._b );

        return this._f( y + this._b );

#end


if __name__ == '__main__' :
    n = Neuron( wc=34  );

    print( n.feedforward([3, 2] ) );
    print( n );
