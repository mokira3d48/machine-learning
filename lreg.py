import numpy 	as np
import pandas 	as pd
import matplotlib.pyplot as plt

# on charge le dataset :
hdata = pd.read_csv('./ressources/house.csv');

# on affiche le nuage de points qui resulte des donnees :
#plt.plot(hdata['surface'], hdata['loyer'], 'ro', markersize=4);
#plt.show();

# on recupere les donnees dont le prix du loyer est inferieur a 10,000 euros :
hdata = hdata[hdata['loyer'] < 10000];

#plt.plot(hdata['surface'], hdata['loyer'], 'ro', markersize=4);
#plt.show();


# on transforme le dataset en matrice :
X = np.matrix([np.ones(hdata.shape[0]), hdata['surface'].values]);
X = X.T;

Y = np.matrix(hdata['loyer']);
Y = Y.T;

# on effectue le calcul exacte du parametre [theta] :
theta = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(Y);

print(theta);

# on represente le model lineaire :
plt.xlabel('Surface');
plt.ylabel('Loyer');
plt.plot(hdata['surface'], hdata['loyer'], 'ro', markersize=4);
plt.plot([0, 250], [theta.item(0), theta.item(0) + 250 * theta.item(1)], linestyle='--', c="#000000");
plt.show();

# prediction du prix de loyer ayant comme surface 35 metres carres :
p35 = theta.item(0) + theta.item(1) * 35;
print(f"For 35m, we have : {p35} euros.");

