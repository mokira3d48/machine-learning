# -*-encoding : utf-8 -*

# author  : Dr Mokira
# created : 2020-06-16
# updated : 2020-06-17

from math import exp
import time
import os

def clear() :
	os.system( 'clear' if os.name != 'nt' else 'cls' );


def sigma( x ) :
	return 1 / ( 1 + exp( -x ) );

def dsigma( x ) :
	return ( 1 - sigma( x )) * sigma( x );

M = [(1,1), (2.5, -1), (1.5, 1.5), (4, -1), (1, 2.5), (4, 2), (2.5, 1), (4, 0.5), (4, 1), (1, -1), (2, -0.5), (2, 1), (2, -1.5), (3, 2) ];
Z = [0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0];
a = 0;
b = 1;
c = -2;

def f( a, b, c, x, y ) :
	return a * x + b * y + c;

N = 100000;
n = 0;

ga = 0;
gb = 0;
gc = 0;

delta = 1;

while n < N :
    ga = 0;
    gb = 0;
    gc = 0;

    clear();
#   print("\033c", end="");
    print("a = %.3f b = %.3f c = %.3f" % (a, b, c));
	
    for i in range(14) :
        yi = f(a, b, c, M[i][0],M[i][1]);
        fi = sigma(yi);
	
        ga += 2 * M[i][0] * dsigma(yi) * ( fi - Z[i]);
        gb += 2 * M[i][1] * dsigma(yi) * ( fi - Z[i]);
        gc += 2 * dsigma(yi) * ( fi - Z[i]);
		
        print("Ai = ( %.2f, %.2f )\tzi = %.3f\tfi = %.3f\t" % (M[i][0], M[i][1] ,Z[i], fi));
	
    ga = ga / 14;
    gb = gb / 14;
    gc = gc / 14;
	
    a = a - delta * ga;
    b = b - delta * gb;
    c = c - delta * gc;
	
    n += 1;
	
    print('itteration : ', n, '\n');
#   input('>_  ');
    time.sleep(0.01);
	
