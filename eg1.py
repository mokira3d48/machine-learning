# -*-encoding : utf-8 -*

# author  : Dr Mokira
# created : 2020-06-17
# updated : 2020-06-17

from math import exp
import time
import os
import random

def clear() :
	os.system( 'clear' if os.name != 'nt' else 'cls' );


def sigma( x ) :
	return 1 / ( 1 + exp( -x ) );

def dsigma( x ) :
	return ( 1 - sigma( x )) * sigma( x );

X = [0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 6.0, 6.2, 6.4, 6.6, 6.8, 7.0, 7.2, 7.4, 7.6, 7.8, 8.0];
Y = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

count = len( X );

print ( f" size X : {count}\t size Y : {len( Y )}" );

a = random.random();
time.sleep( 0.1 );
b = random.random();
time.sleep( 0.1 );
c = random.random();
time.sleep( 0.1 );
d =  random.random();

time.sleep( 0.1 );
e =  random.random();

time.sleep( 0.1 );
f =  random.random();

time.sleep( 0.1 );
g =  random.random();


def n10( a, b, c, x, y ) :
    return a * x + b * y + c;

def n00( d, e, x ) :
    return d * x + e;

def n01( f, g, x ) :
    return f * x + g;


epoch = 5000;
n     = 0;

delta = 1;

while n < epoch :
    ga = 0;
    gb = 0;
    gc = 0;
    gd = 0;
    ge = 0;
    gf = 0;
    gg = 0;

    clear();
#   print("\033c", end="");
    print("a = %.3f b = %.3f c = %.3f d = %.3f e = %.3f f = %.3f g = %.3f" % (a, b, c, d, e, f, g));
	
    for i in range( count ) :
        y00 = n00( d, e, X[i] );
        y01 = n01( f, g, X[i] );
        y10 = n10( a, b, c, sigma( y00 ), sigma( y01 ) );

        fi = sigma(y10);
        Ei = fi - Y[i];

        gaf =  sigma( y00 ) * dsigma( y10 );
        gbf =  sigma( y01 ) * dsigma( y10 );

        ga += 2 * gaf * Ei;
        gb += 2 * gbf * Ei;
        gc += 2 * dsigma( y10 ) * Ei;
        gd += 2 * X[i] * ( dsigma( y00 ) / sigma( y00 ) ) * a * gaf * Ei;
        ge += 2 *        ( dsigma( y00 ) / sigma( y00 ) ) * a * gaf * Ei;
        gf += 2 * X[i] * ( dsigma( y01 ) / sigma( y01 ) ) * b * gbf * Ei;
        gg += 2 *        ( dsigma( y01 ) / sigma( y01 ) ) * b * gbf * Ei;

#        a = a - delta * ga;
#        b = b - delta * gb;
#        c = c - delta * gc;
#        d = d - delta * gd;
#        e = e - delta * ge;
#        f = f - delta * gf;
#        g = g - delta * gg;
		
        print("Xi = %.2f\tYi = %.3f\tfi = %.3f\tEi = %.6f" % (X[i], Y[i], fi, Ei) );
	
    ga = ga / count;
    gb = gb / count;
    gc = gc / count;
    gd = gd / count;
    ge = ge / count;
    gf = gf / count;
    gg = gg / count;


    a = a - delta * ga;
    b = b - delta * gb;
    c = c - delta * gc;
    d = d - delta * gd;
    e = e - delta * ge;
    f = f - delta * gf;
    g = g - delta * gg;
	
    n += 1;
	
    print('Epoch : ', n, '\n');
#    input('>_  ');
    time.sleep( 0.05 );
	
